# -*- coding: utf-8 -*-
def  index():
	pedido_id = request.args(0)
	pedido = db( db.pedido.id == pedido_id, db.viaje.id == db.pedido.viaje_id ).select( db.pedido.ALL, orderby = db.pedido.id ).first()
	productos = db( ).select( db.producto.ALL, orderby = db.producto.descripcion )

	if request.post_vars:
		precio = float(db( db.producto.id == request.post_vars.producto_id ).select( db.producto.precio ).first())
		db.detalle_pedido.insert( pedido_id = request.post_vars.pedido_id, cantidad = request.post_vars.cantidad, producto_id = request.post_vars.producto_id, costo = precio )
		redirect( URL('detalle_pedido','index'))

	return dict( pedido = pedido, productos = productos )

def editar():
	personas = db().select( db.persona.id, db.persona.nombre, orderby = db.persona.nombre )

	pedido_id = request.args(0)
	# row_persona=db.persona(persona_id)
	pedido = db( db.pedido.id == pedido_id ).select( db.pedido.ALL, orderby = db.pedido.id ).first()

	if request.post_vars:
		#Actualizar viaje
		db( db.pedido.id == pedido_id ).update( persona_id = request.post_vars.persona_id, monto = request.post_vars.monto, comision = request.post_vars.comision )
		redirect( URL('pedido','index'))

	return dict( pedido = pedido, personas = personas )

def eliminar():
	viaje_id = request.post_vars.viaje_id
	db( db.viaje.id == viaje_id ).delete()

	return True