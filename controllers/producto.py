# -*- coding: utf-8 -*-
def  index():
	productos = db().select( db.producto.id, db.producto.descripcion, db.producto.precio, orderby=db.producto.descripcion )
	return dict( productos = productos )

def crear():
	if request.post_vars:
		#insertar producto
		db.producto.insert( descripcion = request.post_vars.descripcion, precio = request.post_vars.precio )
	return dict()

def editar():
	producto_id = request.args(0)
	# row_producto=db.producto(producto_id)
	row_producto = db( db.producto.id == producto_id ).select( db.producto.ALL ).first()

	if request.post_vars:
		#Actualizar producto
		db( db.producto.id == producto_id ).update( descripcion = request.post_vars.nombre, precio = request.post_vars.precio )
		redirect( URL('producto','index'))

	return dict( producto = row_producto )

def eliminar():
	producto_id = request.post_vars.producto_id
	db( db.producto.id == producto_id ).delete()

	return True