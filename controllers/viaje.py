# -*- coding: utf-8 -*-
def  index():
	viaje = db().select( db.viaje.id, db.viaje.persona_id, db.viaje.fecha, orderby = db.viaje.id )
	return dict( viaje = viaje )

def crear():
	if request.post_vars:
		#insertar persona
		db.viaje.insert( persona_id = request.post_vars.persona_id )
		redirect( URL('viaje','index'))

	personas = db().select( db.persona.id, db.persona.nombre, orderby = db.persona.nombre )

	return dict( personas = personas )

def editar():
	personas = db().select( db.persona.id, db.persona.nombre, orderby = db.persona.nombre )

	viaje_id = request.args(0)
	# row_persona=db.persona(persona_id)
	row_viaje = db( db.viaje.id == viaje_id ).select( db.viaje.ALL ).first()

	if request.post_vars:
		#Actualizar viaje
		db( db.viaje.id == viaje_id ).update( persona_id = request.post_vars.persona_id, fecha = request.post_vars.fecha )
		redirect( URL('viaje','index'))

	return dict( viaje = row_viaje, personas = personas )

def eliminar():
	viaje_id = request.post_vars.viaje_id
	db( db.viaje.id == viaje_id ).delete()

	return True