# -*- coding: utf-8 -*-
def  index():
	personas = db().select( db.persona.id, db.persona.nombre, orderby=db.persona.nombre )
	return dict( personas = personas )

def crear():
	if request.post_vars:
		#insertar persona
		db.persona.insert( nombre = request.post_vars.nombre )
	return dict()

def editar():
	persona_id = request.args(0)
	# row_persona=db.persona(persona_id)
	row_persona = db( db.persona.id == persona_id ).select( db.persona.ALL ).first()

	if request.post_vars:
		#Actualizar persona
		db( db.persona.id == persona_id ).update( nombre = request.post_vars.nombre )
		redirect( URL('persona','index'))

	return dict( persona = row_persona )

def eliminar():
	persona_id = request.post_vars.persona_id
	db( db.persona.id == persona_id ).delete()

	return True