# -*- coding: utf-8 -*-
def  index():
	pedido = db().select( db.pedido.ALL, orderby = db.pedido.id )
	return dict( pedido = pedido )

def crear():
	if request.post_vars:
		#variable = request.post_vars.viaje_id.split("|")
		#viaje_id = variable[0]
		#persona_id = variable[1]
		#insertar persona
		db.pedido.insert( persona_id = request.post_vars.persona_id, viaje_id = request.post_vars.viaje_id, monto = request.post_vars.monto, comision = request.post_vars.comision )
		redirect( URL('pedido','index'))

	viaje = db().select( db.viaje.id, db.viaje.persona_id, orderby = db.viaje.id )
	personas = db().select( db.persona.id, db.persona.nombre, orderby = db.persona.nombre )

	return dict( viaje = viaje, personas = personas )

def editar():
	personas = db().select( db.persona.id, db.persona.nombre, orderby = db.persona.nombre )

	pedido_id = request.args(0)
	# row_persona=db.persona(persona_id)
	pedido = db( db.pedido.id == pedido_id ).select( db.pedido.ALL, orderby = db.pedido.id ).first()

	if request.post_vars:
		#Actualizar viaje
		db( db.pedido.id == pedido_id ).update( persona_id = request.post_vars.persona_id, monto = request.post_vars.monto, comision = request.post_vars.comision )
		redirect( URL('pedido','index'))

	return dict( pedido = pedido, personas = personas )

def eliminar():
	viaje_id = request.post_vars.viaje_id
	db( db.viaje.id == viaje_id ).delete()

	return True